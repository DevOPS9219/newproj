import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.listeners.Allurelistener;

import org.testng.AssertJUnit;
import org.testng.Assert;
import org.testng.annotations.Test;

import Tests.BaseClass;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.Story;


@Listeners({Allurelistener.class})
public class Maintest extends BaseClass{







@Test(priority=1,description="Search For Strategic Partnership page",timeOut=600000,enabled=true,groups="Smoke")
@Description("Check if User can Search For Strategic Partnership  page")
@Epic("EP001:HRDF_ExternalPortal")
@Feature("Feature 5:TS_005_Main_Page->Programs")
@Story("Story 4:TC_050_Programs_StategicPartnership")
@Step("Check For Strategic Partnership Page")
@Severity(SeverityLevel.NORMAL)
public void checkaboutstratpart()
{   
   System.out.println(driver.getTitle());
}

}

