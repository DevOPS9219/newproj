package Tests;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.chrome.*;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseClass {
	
	
	public WebDriver driver;
	public static ThreadLocal<WebDriver> tdriver = new ThreadLocal<WebDriver>();
	
	
	
	
	
	@BeforeClass
	public void setup()
	{
	    Browserfactory bf = new Browserfactory();
		driver = bf.initialize_driver();
		driver.manage().window().maximize();
		driver.get("https://stage.hrdf.org.sa/");
		
		
		
		
		
		
		
	}
	
	
	
	
	

	
	
	@AfterClass
	public void teardown()
	{
		
		driver.quit();
	}
	
	
	
	
	

}
